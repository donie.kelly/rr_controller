# v1.0.6 (04-08-2021 Firmware Version 7)
- Rotary encoders now supported

# v1.0.5b (04-08-2021 Firmware Version 6) *HOTFIX
- Hat 1 and 2 now performing normally for atmega32u4

# v1.0.5 (11-06-2021 Firmware Version 6)
- More secondary, non-HID devices now supported
    - Atmega328 (Arduino Uno)
    - Atmega2560 (Arduino Mega) *66 inputs on this monster

# v1.0.4 (26-04-2021 Firmware Version 5)
- Button matrices of up to 16x16 are now supported (currently supported microcontrollers support 16 input pins max so 8x8 for now)

- Outputs are partially implemented, a pin can now be assigned to permananently provide 5v, flash or pulse.

# v1.0.2 (29-03-2021 Firmware Version 4)

- ESP32 compatibility implemented
    - ESP32 uses bluetooth HID (usb still required for configuration)
    - ESP32 only works as master device, cannot be slave. Supports Atmega32U4 slaves as usual.

- 8 default analog axis are now fixed, as changing HID identifier packet requires removing and resubscribing bluetooth device

# v1.0.1 (09-02-2021) Firmware Version 3

- increase possible inputs
    - max button inputs: 128 (up from 32)
    - hat inputs: 2 x 8 way hats

- labels for Atmega32u4 changed to reflect Pro Micro pin labels

- Joystick.h library no longer built in, need to download from https://github.com/MHeironimus/ArduinoJoystickLibrary and include in Arduino/libraries for now 

# v1.0.0 (07-02-2021) Firmware Version 2

- Initial release

