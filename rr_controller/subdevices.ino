#include <Wire.h>

// Request Type Codes
#define I2C_REQUEST_INPUT_COUNT 255
#define I2C_REQUEST_INPUT_CONFIG 0
#define I2C_REQUEST_MATRIX_INPUT_CONFIG 4
#define I2C_REQUEST_ENCODER_CONFIG 9
#define I2C_REQUEST_INPUT_VALUES 1
#define I2C_REQUEST_MATRIX_STATE_WITH_ASSIGNMENTS 6
#define I2C_REQUEST_FULL_INPUT_VALUES 2
#define I2C_REQUEST_MATRIX_INPUT_VALUES 5
#define I2C_REQUEST_ENCODER_INPUT_VALUES 7
#define I2C_REQUEST_REPORT_ID 254
#define I2C_REQUEST_JOYSTICK_STATUS 251
#define I2C_INPUT_UPDATE_CONFIG 253
#define I2C_INPUT_MATRIX_UPDATE_CONFIG 247
#define I2C_INPUT_ENCODER_UPDATE_CONFIG 244
#define I2C_INPUT_UPDATE_DEVICE_NAME 249
#define I2C_INPUT_UPDATE_DEVICE_ADDRESS 248
#define I2C_SAVE_INPUTS_TO_EEPROM 252
#define I2C_REQUEST_TEST_DATA 250
#define I2C_REQUEST_RESET_TO_DEFAULTS 246

#define i2c_data_size 32

int requestType = 0; // stores the request type
int requestIndex = 0;

int knownSubDevices[32];

uint8_t i2c_data[i2c_data_size];

void InitI2C()
{
    if (i2c_address == 0)
    {
        delay(2000); // give subdevices a chance to boot first
        Wire.begin();
        Wire.setClock(200000);

        UpdateSubdevices();
    }
    else
    {
        ClearKnownDevices();
        Wire.begin(i2c_address);
    }

#if !defined(ESP32)
    Wire.onRequest(requestEvent); // register event
    Wire.onReceive(receiveEvent); // register event
#endif
}

int SubDeviceCount()
{
    for (int i = 0; i < 32; i++)
    {
        if (knownSubDevices[i] == -1)
        {
            return i;
        }
    }
    return 32;
}

void UpdateSubdevices()
{
    DetectSubDevices();
    //     if (address == 0 && millis() - lastUpdate > 3000)
    //     {
    //         DetectSubDevices();
    //         lastUpdate = millis();

    //     }
}

void ClearKnownDevices()
{
    for (int i = 0; i < 32; i++)
    {
        knownSubDevices[i] = -1;
    }
}

void DetectSubDevices()
{
    ClearKnownDevices();

    if (i2c_address != 0)
    {
        return;
    }

    int deviceCount = 0;

    for (int addr = 1; addr < 127; addr++)
    {
        Wire.beginTransmission(addr);
        int error = Wire.endTransmission();

        if (error == 0)
        {
            knownSubDevices[deviceCount] = addr;
            deviceCount++;
        }
        else if (error == 4)
        {
        }
    }
}

bool IsMaster()
{
    return i2c_address == 0;
}

/* SLAVE RECEIVE EVENTS */

void requestEvent()
{
    //Serial.println("requestEvent");
    if (requestType == I2C_REQUEST_TEST_DATA)
    {
        for (int i = 0; i < 32; i++)
        {
            Wire.write(i);
        }
    }
    else if (requestType == I2C_REQUEST_INPUT_COUNT)
    {
        Wire.write(InputCount());
    }
    else if (requestType == I2C_REQUEST_INPUT_VALUES)
    {
        for (int i = requestIndex; i < requestIndex + 8; i++)
        {
            i2cReportValues(i);
        }
    }
    else if (requestType == I2C_REQUEST_ENCODER_INPUT_VALUES)
    {
        i2cReportEncoderValues();
    }
    else if (requestType == I2C_REQUEST_MATRIX_STATE_WITH_ASSIGNMENTS)
    {
        i2cReportMatrixStatesWithAssignments(requestIndex);
    }
    else if (requestType == I2C_REQUEST_FULL_INPUT_VALUES)
    {
        for (int i = requestIndex; i < requestIndex + 8; i++)
        {
            i2cReportFullValues(i);
        }
    }
    else if (requestType == I2C_REQUEST_MATRIX_INPUT_VALUES)
    {
        // In Inputs.ino
        ReportMatrixStateI2c(0, 16);
    }
    else if (requestType == I2C_REQUEST_REPORT_ID)
    {
        i2cReportID();
    }
    else if (requestType == I2C_REQUEST_INPUT_CONFIG)
    {
        i2cReportConfig(requestIndex);
    }
    else if (requestType == I2C_REQUEST_ENCODER_CONFIG)
    {
        i2cReportEncoderConfig(requestIndex);
    }
    else if (requestType == I2C_REQUEST_MATRIX_INPUT_CONFIG)
    {
        i2cReportMatrixConfig(requestIndex);
    }
}

void i2cWrite(byte b)
{
    Wire.write(b);
}

void i2cReportConfig(int idx)
{

    Wire.write(GetInput(idx).GetPin());
    Wire.write(GetInput(idx).GetPinMode());
    Wire.write(GetInput(idx).IsAnalog());
    Wire.write(GetInput(idx).GetIsInverted());

    Wire.write(highByte(GetInput(idx).GetMinVal()));
    Wire.write(lowByte(GetInput(idx).GetMinVal()));

    Wire.write(highByte(GetInput(idx).GetCenterVal()));
    Wire.write(lowByte(GetInput(idx).GetCenterVal()));

    Wire.write(highByte(GetInput(idx).GetMaxVal()));
    Wire.write(lowByte(GetInput(idx).GetMaxVal()));

    Wire.write(highByte(GetInput(idx).GetDeadZone()));
    Wire.write(lowByte(GetInput(idx).GetDeadZone()));

    Wire.write(GetInput(idx).GetAssignedInput());
}

void i2cReportEncoderConfig(int idx)
{
    Wire.write(GetEncoder(idx).pinAidx);
    Wire.write(GetEncoder(idx).pinBidx);
    Wire.write(GetEncoder(idx).leftAssignment);
    Wire.write(GetEncoder(idx).rightAssignment);
    Wire.write(GetEncoder(idx).pinA);
    Wire.write(GetEncoder(idx).pinB);
    Wire.write(false);
    Wire.write(false);
    Wire.write(false);
    Wire.write(false);
}

void i2cReportMatrixConfig(int idx)
{
    if (idx == 0)
    {
        for (int i = 0; i < 16; i++)
        {
            Wire.write(GetMatrixRowPin(i));
        }
        for (int i = 0; i < 16; i++)
        {
            Wire.write(GetMatrixColPin(i));
        }
    }
    else
    {
        for (int i = 0; i < 32; i++)
        {
            Wire.write(GetMatrixInputAssignment((idx - 1) * 32 + i));
        }
    }
}

void i2cReportID()
{
    for (int i = 0; i < 16; i++)
    {
        Wire.write(deviceName[i]);
    }
    Wire.write(firmwareVersion);
    Wire.write(GetDeviceType());
    Wire.write(i2c_address);
}

// report specific inputs values to master
void i2cReportValues(int idx)
{
    if (idx >= InputCount())
    {
        Wire.write(0);
        Wire.write(0);
        Wire.write(0);
    }
    else
    {
        // Wire.write(highByte(GetInput(idx).GetRawVal()));
        // Wire.write(lowByte(GetInput(idx).GetRawVal()));
        Wire.write(GetInput(idx).GetAssignedInput());
        Wire.write(highByte(GetInput(idx).GetVal()));
        Wire.write(lowByte(GetInput(idx).GetVal()));
    }
}

void i2cReportEncoderValues()
{
    for (int i = 0; i < 4; i++)
    {

        Wire.write(GetEncoder(i).leftAssignment);
        Wire.write(GetEncoder(i).leftDown);

        Wire.write(GetEncoder(i).rightAssignment);
        Wire.write(GetEncoder(i).rightDown);
    }
}

void i2cReportMatrixStatesWithAssignments(int idx)
{
    // if past col count, send 255, 0, 0, 0.... to indicate end of matrix packet
    if (idx >= GetMatrixColCount())
    {
        Wire.write(255);
        for (int i = 0; i < 18; i++)
        {
            Wire.write(false);
        }
        return;
    }

    for (int z = idx; z < idx + 1; z++)
    {
        ReportMatrixStateI2c(z, 1);
        for (int i = 0; i < 16; i++)
        {
            Wire.write(GetMatrixInputAssignment(i + z * 16));
        }
    }
}

void i2cReportFullValues(int idx)
{
    if (idx >= InputCount())
    {
        Wire.write(0);
        Wire.write(0);
        Wire.write(0);
    }
    else
    {
        Wire.write(highByte(GetInput(idx).GetRawVal()));
        Wire.write(lowByte(GetInput(idx).GetRawVal()));
        Wire.write(highByte(GetInput(idx).GetVal()));
        Wire.write(lowByte(GetInput(idx).GetVal()));
    }
}

void UpdateInputConfigFromI2CData()
{
    for (int i = 0; i < 32; i++)
    {
        Serial.println(i2c_data[i]);
    }
    SetInput(i2c_data[2], 2, i2c_data[4]); // pinMode
    SetInput(i2c_data[2], 3, i2c_data[5]); // isAnalog
    SetInput(i2c_data[2], 9, i2c_data[6]); // isInverted

    uint8_t hb = i2c_data[7];
    uint8_t lb = i2c_data[8];
    uint16_t v = (hb << 8) + lb;
    SetInput(i2c_data[2], 5, v); // minval

    hb = i2c_data[9];
    lb = i2c_data[10];
    v = (hb << 8) + lb;
    SetInput(i2c_data[2], 6, v); // midval

    hb = i2c_data[11];
    lb = i2c_data[12];
    v = (hb << 8) + lb;
    SetInput(i2c_data[2], 7, v); // maxval

    hb = i2c_data[13];
    lb = i2c_data[14];
    v = (hb << 8) + lb;
    SetInput(i2c_data[2], 8, v); // deadzone

    SetInput(i2c_data[2], 4, i2c_data[15]); // assignedInput
}

void UpdateInputEncoderConfigFromI2CData()
{
    // for (int i = 0; i < 14; i++){
    //     Serial.print(i);
    //     Serial.print(" ");
    //     Serial.println(i2c_data[i]);
    // }

    SetEncoder(i2c_data[2], 1, i2c_data[3]); // pin a idx
    SetEncoder(i2c_data[2], 2, i2c_data[4]); // pin b idx
    SetEncoder(i2c_data[2], 5, i2c_data[5]); // left assignment
    SetEncoder(i2c_data[2], 6, i2c_data[6]); // right assignment
}

void UpdateInputMatrixConfigFromI2CData()
{
    // i2c_data[0] = I2C_INPUT_MATRIX_UPDATE_CONFIG
    // i2c_data[1] = matrix_update_index 0=rowPins, 1=colpins, 2-11=pinAssignments

    // Recieve Row Pin Assignments
    if (i2c_data[1] == 0)
    {
        Serial.println("Receving rows");
        for (int i = 2; i < 2 + 16; i++)
        {
            SetMatrixRowPin(i - 2, i2c_data[i]);
            Serial.println(i2c_data[i]);
        }
    }
    // Receive Col Pin Assignments
    else if (i2c_data[1] == 1)
    {
        Serial.println("Receving cols");
        for (int i = 2; i < 2 + 16; i++)
        {
            SetMatrixColPin(i - 2, i2c_data[i]);
            Serial.println(i2c_data[i]);
        }

        // Receive 256 input assignments in 9 packets of 30
    }
    else
    {
        Serial.println("Receving assignments");
        for (int i = 2; i < 32; i++)
        {
            SetMatrixInputAssignment((i2c_data[1] - 2) * 30 + (i - 2), i2c_data[i]);
            Serial.println(i2c_data[i]);
        }
    }
}

// respond to data request from master
void receiveEvent(int byteCount)
{
    ClearI2CData();
    uint8_t count = 0;

    //Serial.println("receiveEvent:");

    while (Wire.available()) // loop through all but the last
    {
        int c = Wire.read(); // receive byte as a character
        i2c_data[count] = c;
        count++;
    }

    if (count >= 2)
    {
        requestType = i2c_data[0];
        requestIndex = i2c_data[1];
        // if (count > 2)
        // {
        //     Serial.print(i2c_data[0]);
        //     Serial.print("\t");
        //     Serial.print(i2c_data[1]);
        //     Serial.print("\t");
        //     Serial.print(count);
        //     Serial.println();

        //     for (int i = 0; i < count; i++)
        //     {
        //         Serial.print(i2c_data[i]);
        //         Serial.print(" ");
        //     }
        //     Serial.println();
        // }

        if (requestType == I2C_INPUT_UPDATE_CONFIG)
        {
            UpdateInputConfigFromI2CData();
        }
        else if (requestType == I2C_INPUT_MATRIX_UPDATE_CONFIG)
        {
            UpdateInputMatrixConfigFromI2CData();
        }
        else if (requestType == I2C_INPUT_ENCODER_UPDATE_CONFIG)
        {
            UpdateInputEncoderConfigFromI2CData();
        }
        else if (requestType == I2C_INPUT_UPDATE_DEVICE_NAME)
        {
            for (int i = 1; i < 17; i++)
            {
                deviceName[i - 1] = i2c_data[i];
            }
        }
        else if (requestType == I2C_INPUT_UPDATE_DEVICE_ADDRESS)
        {
            i2c_address = i2c_data[1];
        }
        else if (requestType == I2C_SAVE_INPUTS_TO_EEPROM)
        {
            SaveCurrentInputConfig();
        }
        else if (requestType == I2C_REQUEST_RESET_TO_DEFAULTS)
        {
            ResetToDefaults();
        }
    }
}

/* MASTER SEND EVENTS */
void GetSubDeviceInputs()
{

    for (int i = 0; i < SubDeviceCount(); i++)
    {
        RequestSubdeviceInputValues(i);
        RequestSubdeviceEncoderValues(i);
        RequestSubdeviceMatrixInputValues(i);
    }
}

// bare minimum values for HID report to increase speed
void RequestSubdeviceInputValues(int subdeviceIndex)
{

    // Request number of inputs from subdevice
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_INPUT_COUNT);
    Wire.write(0);
    Wire.endTransmission();

    Wire.requestFrom(knownSubDevices[subdeviceIndex], 1);

    uint8_t inputCount = Wire.read();

    // Request input value packets in groups of 8 inputs (24 bytes)
    for (int i = 0; i < inputCount / 8; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_INPUT_VALUES);
        Wire.write(8 * i); // input start index
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 24);
        uint8_t b0 = 0; // assigned input axis
        uint8_t b1 = 0; // value high byte
        uint8_t b2 = 0; // value low byte
        uint16_t value = 0;
        while (Wire.available())
        {
            b0 = Wire.read();
            b1 = Wire.read();
            b2 = Wire.read();

            if (b0 == 0)
            {
                continue;
            }

            value = (b1 << 8) + b2;

            JoystickInput(b0, value);
        }
    }
}

void RequestSubdeviceEncoderValues(int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_ENCODER_INPUT_VALUES);
    Wire.write(false);
    Wire.endTransmission();

    Wire.requestFrom(knownSubDevices[subdeviceIndex], 16);

    uint8_t assignment = 0;
    uint8_t value = 0;
    while (Wire.available())
    {
        assignment = Wire.read();
        value = Wire.read();
        // Serial.print("E\t");
        // Serial.print(assignment);
        // Serial.print("\t");
        // Serial.println(value);
        JoystickInput(assignment, value);
    }
    //Serial.println("----");
}

void RequestSubdeviceMatrixInputValues(int subdeviceIndex)
{
    for (int i = 0; i < 16; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_MATRIX_STATE_WITH_ASSIGNMENTS);
        Wire.write(i); // input start index
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 18);

        uint8_t b0;
        uint8_t buttonStates0 = 0;
        uint8_t buttonStates1 = 0;

        uint8_t x = 0;
        uint8_t y = 0;
        while (Wire.available())
        {

            buttonStates0 = Wire.read();
            if (buttonStates0 == 255)
            {
                return;
            }
            buttonStates1 = Wire.read();

            for (int b = 0; b < 16; b++)
            {
                b0 = Wire.read();
                if (b < 8)
                {
                    JoystickInput(b0, bitRead(buttonStates0, b));
                }
                else
                {
                    JoystickInput(b0, bitRead(buttonStates1, b - 8));
                }
            }

            // b1 = Wire.read();
            // b2 = Wire.read();
            // value = (b1 << 8) + b2;

            // JoystickInput(b0, value);
            // count++;
        }
    }
}

// full set of values required by configurator app
void PrintSubdeviceInputValuesFull(int subdeviceIndex)
{
    // Request number of inputs from subdevice
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_INPUT_COUNT);
    Wire.write(0);
    Wire.endTransmission();

    Wire.requestFrom(knownSubDevices[subdeviceIndex], 1);

    uint8_t inputCount = Wire.read();

    // Request input value packets in groups of 8 inputs (24 bytes)
    for (int i = 0; i < inputCount / 8; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_FULL_INPUT_VALUES);
        Wire.write(8 * i); // input start index
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 32);
        int count = 0;
        uint8_t b = 0;
        while (Wire.available())
        {
            // 4 bytes incoming
            // high and low bytes for raw val
            // high and low bytes for calibrated val
            for (int z = 0; z < 4; z++)
            {
                b = Wire.read();
                // if (z == 1 || z == 3)
                // {
                if (b == 10 || b == 13)
                {
                    b += 1;
                }
                //}
                Serial.write(b);
            }

            // cancel after 16 inputs received
            count++;
            if (count == 16)
            {
                return;
            }
        }
    }
}

void PrintSubDeviceMatrixState(int subdeviceIndex)
{
    for (int i = 0; i < 2; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_MATRIX_INPUT_VALUES);
        Wire.write(0); // input start index
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 32);
        while (Wire.available())
        {
            Serial.write(Wire.read());
        }
    }
}

void PrintSubdeviceDeviceID(int subdeviceIndex)
{
    // for (int i = 0; i < 2; i++)
    // {
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_REPORT_ID);
    Wire.write(0);
    Wire.endTransmission();

    Wire.requestFrom(knownSubDevices[subdeviceIndex], 19);
    while (Wire.available())
    {
        Serial.write(Wire.read()); // pass directly to serial
    }
    //}
}

void PrintSubdeviceInputConfig(int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_INPUT_COUNT);
    Wire.write(0);
    Wire.endTransmission();

    Wire.requestFrom(knownSubDevices[subdeviceIndex], 1);

    uint8_t inputCount = Wire.read();

    Serial.write(inputCount); // input_count asdf

    for (int i = 0; i < inputCount; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_INPUT_CONFIG);
        Wire.write(i);
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 13);

        while (Wire.available())
        {
            Serial.write(Wire.read()); // pass directly to serial
        }
    }

    // Request Encoder config from subdevice
    for (int i = 0; i < 4; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_ENCODER_CONFIG);
        Wire.write(i);
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 10);

        while (Wire.available())
        {
            Serial.write(Wire.read()); // pass directly to serial
        }
    }

    // Request Matrix config from subdevice
    for (int i = 0; i < 9; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_REQUEST_MATRIX_INPUT_CONFIG);
        Wire.write(i);
        Wire.endTransmission();

        Wire.requestFrom(knownSubDevices[subdeviceIndex], 32);

        while (Wire.available())
        {
            Serial.write(Wire.read()); // pass directly to serial
        }
    }
}

void SendConfigUpdate(char *data, int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_INPUT_UPDATE_CONFIG);

    for (int i = 1; i < 16; i++) // Length of GPIO config update packet
    {
        Wire.write(data[i]);
    }

    Wire.endTransmission();
}

void SendEncoderConfigUpdate(char *data, int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_INPUT_ENCODER_UPDATE_CONFIG);

    for (int i = 0; i < 14; i++) // Length of GPIO config update packet
    {
        Wire.write(data[i]);
    }

    Wire.endTransmission();
}

void ResetSubDeviceToDefaults(int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_REQUEST_RESET_TO_DEFAULTS);
    Wire.write(0);
    Wire.endTransmission();
}

void SendMatrixConfigUpdate(char *data, int subdeviceIndex)
{
    for (int i = 0; i < 11; i++)
    {
        Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
        Wire.write(I2C_INPUT_MATRIX_UPDATE_CONFIG);
        Wire.write(i);
        // Send Row Pin Assignments
        if (i == 0)
        {
            for (int x = 0; x < 16; x++)
            {
                Wire.write(data[1 + x]);
                //Serial.write(data[1+x]);
            }
        }
        // Send Col Pin Assignments
        else if (i == 1)
        {
            for (int x = 0; x < 16; x++)
            {
                Wire.write(data[1 + x + 16]);
                //Serial.write(data[1 + x + 16]);
            }
        }
        // Send 256 input assignments in 9 packets of 30
        else
        {
            for (int x = 0; x < 30; x++)
            {
                Wire.write(data[1 + 32 + x + (i - 2) * 30]);
                //Serial.write(data[1 + 32 + x + (i-2)*30]);
            }
        }

        Wire.endTransmission();
    }
}

void SendUpdateDeviceName(char *data, int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_INPUT_UPDATE_DEVICE_NAME);

    // Send 16 char device name
    for (int i = 1; i < 17; i++)
    {
        Wire.write(data[i]);
    }

    Wire.endTransmission();
}

void SendUpdateDeviceAddress(int val, int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_INPUT_UPDATE_DEVICE_ADDRESS);

    Wire.write(val);

    Wire.endTransmission();
}

void OrderSaveToEEPROM(int subdeviceIndex)
{
    Wire.beginTransmission(knownSubDevices[subdeviceIndex]);
    Wire.write(I2C_SAVE_INPUTS_TO_EEPROM);
    Wire.write(0);
    Wire.endTransmission();
}

void ClearI2CData()
{
    for (int i = 0; i < i2c_data_size; i++)
    {
        i2c_data[i] = 0;
    }
}
